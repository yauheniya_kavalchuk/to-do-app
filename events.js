import { ToDoList } from "./todolistClass";
import { renderToDoList } from "./todolist";

function changeFormVisibility(e) {
  if(e) {
    const clickEvent = e.target.getAttribute("id") === "add";
    const keyPressEvent = e.ctrlKey && e.key === '\x11';

    if(!clickEvent && !keyPressEvent) {
      return;
    }
    e.preventDefault();
  }

  const form = document.getElementById("form");
  const formClass = form.className;
  const input = form.getElementsByTagName("input")[0];
  input.disabled = true;

  if (!formClass) {
    form.className = "hidden";
    input.value = "";
    input.blur();
  } else {
    form.className = "";
    input.disabled = false;
    input.focus();
  }
}

function deleteItem(e) {
  if (e.target.className !== "delete") {
    return;
  }

  const item = e.target.parentElement;
  const itemID = +item.dataset.id;
  const toDoList = new ToDoList(false);
  toDoList.removeItemByID(itemID);
  renderToDoList(toDoList.getFromStorage());
}

function addNewItem(e) {
  e.preventDefault();

  const toDoList = new ToDoList(false);
  const input = document.getElementsByTagName("input")[0];
  const text = input.value;
  if (text) {
    toDoList.pushItem(text);
    renderToDoList(toDoList.getFromStorage());
    changeFormVisibility();
    input.value = "";
  }
}

export { changeFormVisibility, addNewItem, deleteItem };
