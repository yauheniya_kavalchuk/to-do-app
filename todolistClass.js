class ToDoList {
  #todos;

  constructor(firstInit) {
    if (firstInit) {
      this.#todos = ["Print pages", "Feed parrot", "Buy bread"];
    } else {
      this.#todos = this.getFromStorage();
    }
    this.reloadStorage();
  }

  get todos() {
    return [...this.#todos];
  }

  set todos(listItems) {
    this.#todos = listItems;
  }

  pushItem(items) {
    this.#todos.push(items);
    this.reloadStorage();
  }

  removeItemByID(itemID) {
    this.todos = this.todos.filter((element, index) => index !== itemID);
    this.reloadStorage();
  }

  reloadStorage() {
    sessionStorage.setItem("todos", JSON.stringify(this.todos));
  }

  getFromStorage() {
    return JSON.parse(sessionStorage.getItem("todos"));
  }
}

export { ToDoList };
