import { createElement } from "./createElement";

function renderToDoList(
  todolistData,
  root = document.getElementById("list-container")
) {
  const oldToDoList = document.querySelector("#container");
  const newToDoList = createToDoList(todolistData);
  if (oldToDoList) {
    root.replaceChild(newToDoList, oldToDoList);
  } else {
    root.append(newToDoList);
  }
}

function createToDoList(todos) {
  const list = createElement("ul", { attributes: { id: "container" } });
  const listItems = todos.map((el, id) => renderItem(el, id));
  list.append(...listItems);
  return list;
}

function renderItem(item, id) {
  const listItem = createElement("li", { attributes: { "data-id": id } });
  const listText = createElement("p", { content: item });
  const delBut = createDeleteButton();
  listItem.append(listText, delBut);
  return listItem;
}

function createDeleteButton() {
  return createElement("button", {
    content: "Delete",
    attributes: { class: "delete" },
  });
}

export { renderToDoList };
