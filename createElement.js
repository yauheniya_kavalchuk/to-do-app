function createElement(tag, { attributes, content }) {
  const element = document.createElement(tag);

  if (attributes) {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in attributes) {
      if (Object.hasOwn(attributes, key)) {
        element.setAttribute(key, attributes[key]);
      }
    }
  }

  if (content) {
    element.textContent = content;
  }

  return element;
}

export { createElement };
