import { createElement } from "./createElement";
import { addNewItem, deleteItem, changeFormVisibility } from "./events";

function renderToDoApp() {
  const root = document.getElementsByTagName("body")[0];
  const content = createStatic();
  root.append(content);
  addEvents();
}

function addEvents() {
  document.addEventListener("click", deleteItem);
  document.addEventListener("click", changeFormVisibility);
  document.addEventListener("keypress", changeFormVisibility);
  document.addEventListener("click", addNewItem);
}

function createStatic() {
  const div = createDiv();
  const h1 = createHeading();
  const container = createListContainer();
  const form = createForm();
  const button = createAddButton();

  form.append(createInput(), createSubmitButton());
  div.append(h1, container, form, button);

  return div;
}

function createDiv() {
  return createElement("div", { attributes: { id: "to-do-app" } });
}

function createHeading() {
  return createElement("h1", { content: "My to-do list" });
}

function createListContainer() {
  const listContainer = createElement("div", {
    attributes: { id: "list-container" },
  });
  return listContainer;
}

function createForm() {
  const form = createElement("form", {
    attributes: { id: "form", class: "hidden" },
  });
  return form;
}

function createInput() {
  return createElement("input", {});
}

function createSubmitButton() {
  return createElement("input", {
    attributes: { type: "submit", value: "OK" },
  });
}

function createAddButton() {
  const addButton = createElement("button", {
    attributes: { id: "add", title: "Ctrl+Q" },
    content: "Add new item",
  });
  return addButton;
}

export { renderToDoApp };
