import { renderToDoApp } from "./renderStatic";
import { renderToDoList } from "./todolist";
import { ToDoList } from "./todolistClass";
import "./styles/style.css";

appInit();

function appInit() {
  renderToDoApp();
  let toDoList = new ToDoList(true);
  renderToDoList(toDoList.todos);
}
